package userstorage

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/user/model"
)

func (s *sqlStore) CreateUser(_ context.Context, data *model.UserCreate) error {
	db := s.db.Begin()
	data.PrepareForInsert()

	if err := db.Table(data.TableName()).Create(data).Error; err != nil {
		db.Rollback()
		return common.ErrDB(err)
	}

	if err := db.Commit().Error; err != nil {
		db.Rollback()
		return common.ErrDB(err)
	}

	return nil
}

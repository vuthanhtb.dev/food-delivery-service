package usermodel

import (
	"errors"
	"food-delivery-service/common"
	"food-delivery-service/plugin/tokenprovider"
)

const EntityName = "User"

var (
	ErrEmailOrPasswordInvalid = common.NewCustomError(
		errors.New("email or password invalid"),
		"email or password invalid",
		"ErrUsernameOrPasswordInvalid",
	)

	ErrEmailExisted = common.NewCustomError(
		errors.New("email has already existed"),
		"email has already existed",
		"ErrEmailExisted",
	)
)

type User struct {
	common.SQLModel `json:",inline"`
	Email           string   `json:"email" gorm:"column:email;"`
	Password        string   `json:"-" gorm:"column:password;"`
	Salt            string   `json:"-" gorm:"column:salt;"`
	LastName        string   `json:"last_name" gorm:"column:last_name;"`
	FirstName       string   `json:"first_name" gorm:"column:first_name;"`
	Phone           string   `json:"phone" gorm:"column:phone;"`
	Role            UserRole `json:"role" gorm:"column:role;"`
}

func (u *User) GetUserId() int {
	return u.Id
}

func (u *User) GetEmail() string {
	return u.Email
}

func (u *User) GetRole() string {
	return u.Role.String()
}

func (User) TableName() string {
	return "users"
}

type Account struct {
	AccessToken  *tokenprovider.Token `json:"access_token"`
	RefreshToken *tokenprovider.Token `json:"refresh_token"`
}

func NewAccount(at, rt *tokenprovider.Token) *Account {
	return &Account{
		AccessToken:  at,
		RefreshToken: rt,
	}
}

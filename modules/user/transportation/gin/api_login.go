package usergin

import (
	"food-delivery-service/common"
	"food-delivery-service/component/hasher"
	business "food-delivery-service/modules/user/business"
	model "food-delivery-service/modules/user/model"
	storage "food-delivery-service/modules/user/storage"
	goservice "food-delivery-service/plugin/gosdk"
	"food-delivery-service/plugin/tokenprovider"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

func Login(sc goservice.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var loginUserData model.UserLogin

		if err := c.ShouldBind(&loginUserData); err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		db := sc.MustGet(common.DBMain).(*gorm.DB)
		tokenProvider := sc.MustGet(common.JWTProvider).(tokenprovider.Provider)

		store := storage.NewSQLStore(db)
		md5 := hasher.NewMd5Hash()

		biz := business.NewLoginBusiness(store, tokenProvider, md5, 60*60*24*30, 60*60*24*30)
		account, err := biz.Login(c.Request.Context(), &loginUserData)

		if err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(account))
	}
}

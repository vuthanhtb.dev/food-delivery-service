package usergin

import (
	"food-delivery-service/common"
	goservice "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Profile(_ goservice.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {

		u := c.MustGet(common.CurrentUser)

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(u))
	}
}

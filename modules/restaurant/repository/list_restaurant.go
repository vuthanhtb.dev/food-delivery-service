package restaurantrepo

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

type ListRestaurantStore interface {
	ListRestaurant(
		ctx context.Context,
		filter *model.Filter,
		paging *common.Paging,
		moreKeys ...string,
	) ([]model.Restaurant, error)
}

type UserStore interface {
	GetUsers(ctx context.Context, ids []int) ([]common.SimpleUser, error)
}

func NewListRestaurantRepo(store ListRestaurantStore, uStore UserStore) *listRestaurantRepo {
	return &listRestaurantRepo{store: store, uStore: uStore}
}

type listRestaurantRepo struct {
	store  ListRestaurantStore
	uStore UserStore
}

func (repo *listRestaurantRepo) ListRestaurant(
	ctx context.Context,
	filter *model.Filter,
	paging *common.Paging,
) ([]model.Restaurant, error) {
	result, err := repo.store.ListRestaurant(ctx, filter, paging)

	if err != nil {
		return nil, common.ErrCannotListEntity(model.EntityName, err)
	}

	userIds := make([]int, len(result))

	for i := range result {
		userIds[i] = result[i].OwnerId
	}

	users, err := repo.uStore.GetUsers(ctx, userIds)

	if err != nil {
		return nil, common.ErrCannotListEntity(model.EntityName, err)
	}

	mapUser := make(map[int]*common.SimpleUser, len(users))

	for j, u := range users {
		mapUser[u.Id] = &users[j]
	}

	for i, item := range result {
		result[i].Owner = mapUser[item.OwnerId]
	}

	return result, nil
}

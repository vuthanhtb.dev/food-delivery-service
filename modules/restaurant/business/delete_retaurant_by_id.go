package restaurantbusiness

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

type DeleteRestaurantStore interface {
	FindRestaurant(
		ctx context.Context,
		cond map[string]interface{},
		moreKeys ...string,
	) (*model.Restaurant, error)

	DeleteRestaurant(
		ctx context.Context,
		cond map[string]interface{},
	) error
}

func NewDeleteRestaurantBusiness(store DeleteRestaurantStore) *deleteRestaurantBusiness {
	return &deleteRestaurantBusiness{store: store}
}

type deleteRestaurantBusiness struct {
	store DeleteRestaurantStore
}

func (business *deleteRestaurantBusiness) DeleteRestaurantById(ctx context.Context, id int) error {
	_, err := business.store.FindRestaurant(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return common.ErrEntityNotFound(model.EntityName, err)
	}

	if err := business.store.DeleteRestaurant(ctx, map[string]interface{}{"id": id}); err != nil {
		return common.ErrCannotDeleteEntity(model.EntityName, err)
	}

	return nil
}

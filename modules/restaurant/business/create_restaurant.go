package restaurantbusiness

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

type CreateRestaurantStore interface {
	InsertRestaurant(ctx context.Context, data *model.RestaurantCreate) error
}

func NewCreateRestaurantBusiness(store CreateRestaurantStore) *createRestaurantBusiness {
	return &createRestaurantBusiness{store: store}
}

type createRestaurantBusiness struct {
	store CreateRestaurantStore
}

func (business *createRestaurantBusiness) CreateRestaurant(ctx context.Context, data *model.RestaurantCreate) error {
	if err := data.Validate(); err != nil {
		return err
	}

	if err := business.store.InsertRestaurant(ctx, data); err != nil {
		return common.ErrCannotCreateEntity(model.EntityName, err)
	}

	return nil
}

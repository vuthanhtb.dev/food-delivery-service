package restaurantbusiness

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

type ListRestaurantRepo interface {
	ListRestaurant(
		ctx context.Context,
		filter *model.Filter,
		paging *common.Paging,
	) ([]model.Restaurant, error)
}

func NewListRestaurantBusiness(repo ListRestaurantRepo) *listRestaurantBusiness {
	return &listRestaurantBusiness{repo: repo}
}

type listRestaurantBusiness struct {
	repo ListRestaurantRepo
}

func (business *listRestaurantBusiness) ListRestaurant(
	ctx context.Context,
	filter *model.Filter,
	paging *common.Paging,
) ([]model.Restaurant, error) {
	result, err := business.repo.ListRestaurant(ctx, filter, paging)

	if err != nil {
		return nil, common.ErrCannotListEntity(model.EntityName, err)
	}

	return result, nil
}

package restaurantbusiness

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

type FindRestaurantStore interface {
	FindRestaurant(
		ctx context.Context,
		cond map[string]interface{},
		moreKeys ...string,
	) (*model.Restaurant, error)
}

func NewFindRestaurantBusiness(store FindRestaurantStore) *findRestaurantBusiness {
	return &findRestaurantBusiness{store: store}
}

type findRestaurantBusiness struct {
	store FindRestaurantStore
}

func (business *findRestaurantBusiness) FindRestaurantById(ctx context.Context, id int) (*model.Restaurant, error) {
	data, err := business.store.FindRestaurant(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return nil, common.ErrEntityNotFound(model.EntityName, err)
	}

	return data, nil
}

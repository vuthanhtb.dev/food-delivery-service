package restaurantbusiness

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

type UpdateRestaurantStore interface {
	FindRestaurant(
		ctx context.Context,
		cond map[string]interface{},
		moreKeys ...string,
	) (*model.Restaurant, error)

	UpdateRestaurant(
		ctx context.Context,
		cond map[string]interface{},
		data *model.RestaurantUpdate,
	) error
}

func NewUpdateRestaurantBusiness(store UpdateRestaurantStore) *updateRestaurantBusiness {
	return &updateRestaurantBusiness{store: store}
}

type updateRestaurantBusiness struct {
	store UpdateRestaurantStore
}

func (business *updateRestaurantBusiness) UpdateRestaurantById(ctx context.Context, id int, data *model.RestaurantUpdate) error {
	_, err := business.store.FindRestaurant(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return common.ErrEntityNotFound(model.EntityName, err)
	}

	if err := business.store.UpdateRestaurant(ctx, map[string]interface{}{"id": id}, data); err != nil {
		return common.ErrCannotUpdateEntity(model.EntityName, err)
	}

	return nil
}

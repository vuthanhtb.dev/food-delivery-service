package restaurantmodel

import (
	"food-delivery-service/common"
	"strings"
)

var (
	ErrNameCannotBeBlank = common.NewCustomError(nil, "restaurant name can't be blank", "ErrNameCannotBeBlank")
)

type RestaurantCreate struct {
	common.SQLModel
	OwnerId int    `json:"owner_id" gorm:"column:owner_id;"`
	Name    string `json:"name" gorm:"column:name;"`
	Addr    string `json:"address" gorm:"column:addr;"`
}

func (RestaurantCreate) TableName() string {
	return Restaurant{}.TableName()
}

func (r *RestaurantCreate) Validate() error {
	r.Id = 0
	r.Name = strings.TrimSpace(r.Name)

	if len(r.Name) == 0 {
		return ErrNameCannotBeBlank
	}

	return nil
}

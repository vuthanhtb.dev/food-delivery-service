package restaurantmodel

import "food-delivery-service/common"

const EntityName = "Restaurant"

type Restaurant struct {
	common.SQLModel
	OwnerId     int                `json:"-" gorm:"column:owner_id;"`
	FakeOwnerId *common.UID        `json:"owner_id" gorm:"-"`
	Name        string             `json:"name" gorm:"column:name;"`
	Addr        string             `json:"address" gorm:"column:addr;"`
	LikedCount  int                `json:"liked_count" gorm:"column:liked_count;"`
	Liked       bool               `json:"liked" gorm:"-"`
	Owner       *common.SimpleUser `json:"owner" gorm:"foreignKey:OwnerId;PRELOAD:false;"`
}

func (r *Restaurant) Mask(isAdminOrOwner bool) {
	r.SQLModel.Mask(common.DbTypeRestaurant)
	fakeOwnerId := common.NewUID(uint32(r.OwnerId), int(common.DbTypeUser), 1)
	r.FakeOwnerId = &fakeOwnerId

	if v := r.Owner; v != nil {
		v.Mask(common.DbTypeUser)
	}
}

func (Restaurant) TableName() string {
	return "restaurants"
}

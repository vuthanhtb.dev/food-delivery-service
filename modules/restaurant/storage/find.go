package restaurantstorage

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
	"gorm.io/gorm"
)

func (store *sqlStore) FindRestaurant(
	_ context.Context,
	cond map[string]interface{},
	moreKeys ...string,
) (*model.Restaurant, error) {
	var data model.Restaurant

	if err := store.db.Where(cond).First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.ErrRecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}

package restaurantstorage

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

func (store *sqlStore) InsertRestaurant(_ context.Context, data *model.RestaurantCreate) error {
	data.PrepareForInsert()
	if err := store.db.Create(data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}

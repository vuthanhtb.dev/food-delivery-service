package restaurantstorage

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurant/model"
)

func (store *sqlStore) DeleteRestaurant(
	_ context.Context,
	cond map[string]interface{},
) error {
	if err := store.db.
		Table(model.Restaurant{}.TableName()).
		Where(cond).Delete(nil).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}

package restaurantgin

import (
	"food-delivery-service/common"
	business "food-delivery-service/modules/restaurant/business"
	storage "food-delivery-service/modules/restaurant/storage"
	service "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

func GetRestaurantHandler(sc service.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		uid, err := common.FromBase58(c.Param("restaurant_id"))

		if err != nil {
			panic(err)
		}

		db := sc.MustGet(common.DBMain).(*gorm.DB)
		store := storage.NewSQLStore(db)

		biz := business.NewFindRestaurantBusiness(store)

		data, err := biz.FindRestaurantById(c.Request.Context(), int(uid.GetLocalID()))

		if err != nil {
			panic(err)
		}

		data.Mask(true)

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data))
	}
}

package restaurantgin

import (
	"food-delivery-service/common"
	business "food-delivery-service/modules/restaurant/business"
	model "food-delivery-service/modules/restaurant/model"
	storage "food-delivery-service/modules/restaurant/storage"
	goservice "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

func CreateRestaurantHandler(sc goservice.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data model.RestaurantCreate

		if err := c.ShouldBind(&data); err != nil {
			panic(err)
		}

		user := c.MustGet(common.CurrentUser).(common.Requester)
		data.OwnerId = user.GetUserId()

		db := sc.MustGet(common.DBMain).(*gorm.DB)
		store := storage.NewSQLStore(db)
		biz := business.NewCreateRestaurantBusiness(store)

		if err := biz.CreateRestaurant(c.Request.Context(), &data); err != nil {
			panic(err)
		}

		data.Mask(common.DbTypeRestaurant)

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data.FakeId.String()))
	}
}

package restaurantgin

import (
	"food-delivery-service/common"
	business "food-delivery-service/modules/restaurant/business"
	model "food-delivery-service/modules/restaurant/model"
	storage "food-delivery-service/modules/restaurant/storage"
	goservice "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

func UpdateRestaurantHandler(sc goservice.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var data model.RestaurantUpdate

		id, err := strconv.Atoi(c.Param("restaurant_id"))

		if err != nil {
			c.JSON(http.StatusBadRequest, common.ErrInvalidRequest(err))
			return
		}

		if err := c.ShouldBind(&data); err != nil {
			c.JSON(http.StatusBadRequest, common.ErrInvalidRequest(err))
			return
		}

		db := sc.MustGet(common.DBMain).(*gorm.DB)
		store := storage.NewSQLStore(db)

		biz := business.NewUpdateRestaurantBusiness(store)

		if err := biz.UpdateRestaurantById(c.Request.Context(), id, &data); err != nil {
			c.JSON(http.StatusBadRequest, err)
			return
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(true))
	}
}

package restaurantgin

import (
	"context"
	"food-delivery-service/common"
	business "food-delivery-service/modules/restaurant/business"
	model "food-delivery-service/modules/restaurant/model"
	repository "food-delivery-service/modules/restaurant/repository"
	storage "food-delivery-service/modules/restaurant/storage"
	goservice "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

func ListRestaurant(sc goservice.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		var paging common.Paging

		if err := c.ShouldBind(&paging); err != nil {
			panic(err)
		}

		var filter model.Filter

		if err := c.ShouldBind(&filter); err != nil {
			panic(err)
		}

		_ = paging.Validate()

		db := sc.MustGet(common.DBMain).(*gorm.DB)
		store := storage.NewSQLStore(db)
		//userStore := userstorage.NewSQLStore(db)
		//userStore := api.NewUserApi("http://localhost:8080")

		userService := sc.MustGet(common.PluginUserService).(interface {
			GetUsers(ctx context.Context, ids []int) ([]common.SimpleUser, error)
		})

		repo := repository.NewListRestaurantRepo(store, userService)

		biz := business.NewListRestaurantBusiness(repo)

		result, err := biz.ListRestaurant(c.Request.Context(), &filter, &paging)

		if err != nil {
			c.JSON(http.StatusBadRequest, err)
			return
		}

		for index := range result {
			result[index].Mask(true)
		}

		c.JSON(http.StatusOK, common.NewSuccessResponse(result, paging, filter))
	}
}

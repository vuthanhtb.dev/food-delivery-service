package restaurantlikebusiness

import (
	"context"
	"food-delivery-service/common"
	"food-delivery-service/component/asyncjob"
	model "food-delivery-service/modules/restaurantlike/model"
	"food-delivery-service/plugin/gosdk/logger"
)

type UserLikeRestaurantStore interface {
	Create(ctx context.Context, data *model.Like) error
	CheckUserLike(ctx context.Context, userId, restaurantId int) (bool, error)
}

type IncLikedCountResStore interface {
	IncreaseLikeCount(ctx context.Context, id int) error
}

type userLikeRestaurantBiz struct {
	store UserLikeRestaurantStore
	//pb    pubsub.Pubsub
	incStore IncLikedCountResStore
}

func NewUserLikeRestaurantBiz(
	store UserLikeRestaurantStore,
	incStore IncLikedCountResStore,
// pb pubsub.Pubsub,
) *userLikeRestaurantBiz {
	return &userLikeRestaurantBiz{
		store: store,
		//pb:    pb,
		incStore: incStore,
	}
}

func (business *userLikeRestaurantBiz) LikeRestaurant(
	ctx context.Context,
	data *model.Like,
) error {
	liked, err := business.store.CheckUserLike(ctx, data.UserId, data.RestaurantId)

	if err != nil && err != common.ErrRecordNotFound {
		return model.ErrCannotLikeRestaurant(err)
	}

	if liked {
		return model.ErrUserAlreadyLikedRestaurant(nil)
	}

	err = business.store.Create(ctx, data)

	if err != nil {
		return model.ErrCannotLikeRestaurant(err)
	}

	// Side effect
	go func() {
		defer common.Recover()
		job := asyncjob.NewJob(func(ctx context.Context) error {
			if err := business.incStore.IncreaseLikeCount(ctx, data.RestaurantId); err != nil {
				logger.GetCurrent().GetLogger("user.like.restaurant").Errorln(err)
				return err
			}

			return nil
		}, asyncjob.WithName("IncreaseLikeCount"))

		if err := asyncjob.NewGroup(false, job).Run(ctx); err != nil {
			logger.GetCurrent().GetLogger("user.like.restaurant").Errorln(err)
		}
	}()

	//newMessage := pubsub.NewMessage(data)
	//business.pb.Publish(ctx, common.TopicUserLikeRestaurant, newMessage)

	return nil
}

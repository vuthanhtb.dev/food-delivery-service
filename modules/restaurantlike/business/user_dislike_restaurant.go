package restaurantlikebusiness

import (
	"context"
	"food-delivery-service/common"
	"food-delivery-service/component/asyncjob"
	model "food-delivery-service/modules/restaurantlike/model"
	"food-delivery-service/plugin/gosdk/logger"
)

type UserDislikeRestaurantStore interface {
	Delete(ctx context.Context, userId, restaurantId int) error
	FindUserLike(ctx context.Context, userId, restaurantId int) (*model.Like, error)
}

type DecLikedCountResStore interface {
	DecreaseLikeCount(ctx context.Context, id int) error
}

type userDislikeRestaurantBusiness struct {
	store    UserDislikeRestaurantStore
	decStore DecLikedCountResStore
	//pb pubsub.Pubsub
}

func NewUserDislikeRestaurantBusiness(
	store UserDislikeRestaurantStore,
	decStore DecLikedCountResStore,
	// pb pubsub.Pubsub,
) *userDislikeRestaurantBusiness {
	return &userDislikeRestaurantBusiness{
		store: store,
		//pb:    pb,
		decStore: decStore,
	}
}

func (business *userDislikeRestaurantBusiness) DislikeRestaurant(
	ctx context.Context,
	userId,
	restaurantId int,
) error {
	oldData, err := business.store.FindUserLike(ctx, userId, restaurantId)

	if oldData == nil {
		return model.ErrCannotDidNotLikeRestaurant(err)
	}

	err = business.store.Delete(ctx, userId, restaurantId)

	if err != nil {
		return model.ErrCannotDislikeRestaurant(err)
	}

	// Side effect
	go func() {
		defer common.Recover()
		job := asyncjob.NewJob(func(ctx context.Context) error {
			if err := business.decStore.DecreaseLikeCount(ctx, restaurantId); err != nil {
				logger.GetCurrent().GetLogger("user.dislike.restaurant").Errorln(err)
				return err
			}

			return nil
		}, asyncjob.WithName("DecreaseLikeCount"))

		if err := asyncjob.NewGroup(false, job).Run(ctx); err != nil {
			logger.GetCurrent().GetLogger("user.dislike.restaurant").Errorln(err)
		}
	}()

	//newMessage := pubsub.NewMessage(map[string]interface{}{"restaurant_id": restaurantId})
	//business.pb.Publish(ctx, common.TopicUserDislikeRestaurant, newMessage)

	//go func() {
	//	defer common.Recover()
	//
	//	if err := business.decStore.DecreaseLikeCount(ctx, restaurantId); err != nil {
	//
	//	}
	//}()

	return nil
}

package restaurantlikestorage

import (
	"context"
	"food-delivery-service/common"
	model "food-delivery-service/modules/restaurantlike/model"
	"gorm.io/gorm"
)

func (s *sqlStore) FindUserLike(ctx context.Context, userId, restaurantId int) (*model.Like, error) {
	var data model.Like

	if err := s.db.
		Where("user_id = ? and restaurant_id = ?", userId, restaurantId).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.ErrRecordNotFound
		}

		return nil, common.ErrDB(err)
	}

	return &data, nil
}

func (s *sqlStore) CheckUserLike(ctx context.Context, userId, restaurantId int) (bool, error) {
	var data model.Like

	if err := s.db.
		Where("user_id = ? and restaurant_id = ?", userId, restaurantId).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, common.ErrRecordNotFound
		}

		return false, common.ErrDB(err)
	}

	return true, nil
}

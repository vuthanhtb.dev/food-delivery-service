package restaurantlikegin

import (
	"food-delivery-service/common"
	restaurant "food-delivery-service/modules/restaurant/storage"
	business "food-delivery-service/modules/restaurantlike/business"
	storage "food-delivery-service/modules/restaurantlike/storage"
	service "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

func UserDislikeRestaurant(sc service.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		uid, err := common.FromBase58(c.Param("restaurant_id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)

		db := sc.MustGet(common.DBMain).(*gorm.DB)

		store := storage.NewSQLStore(db)
		decStore := restaurant.NewSQLStore(db)
		biz := business.NewUserDislikeRestaurantBusiness(store, decStore)

		if err := biz.DislikeRestaurant(c.Request.Context(), requester.GetUserId(), int(uid.GetLocalID())); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(true))
	}
}

package restaurantlikegin

import (
	"food-delivery-service/common"
	restaurant "food-delivery-service/modules/restaurant/storage"
	business "food-delivery-service/modules/restaurantlike/business"
	model "food-delivery-service/modules/restaurantlike/model"
	storage "food-delivery-service/modules/restaurantlike/storage"
	service "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

func UserLikeRestaurant(sc service.ServiceContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		uid, err := common.FromBase58(c.Param("restaurant_id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)

		data := model.Like{
			RestaurantId: int(uid.GetLocalID()),
			UserId:       requester.GetUserId(),
		}

		db := sc.MustGet(common.DBMain).(*gorm.DB)

		store := storage.NewSQLStore(db)
		incStore := restaurant.NewSQLStore(db)
		biz := business.NewUserLikeRestaurantBiz(store, incStore)

		if err := biz.LikeRestaurant(c.Request.Context(), &data); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(true))
	}
}

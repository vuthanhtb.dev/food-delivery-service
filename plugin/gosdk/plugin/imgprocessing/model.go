package imgprocessing

import "food-delivery-service/plugin/gosdk/sdkcm"

type Response struct {
	sdkcm.AppError
	Data *sdkcm.Image `json:"data"`
}

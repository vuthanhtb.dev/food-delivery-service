package middleware

import (
	"food-delivery-service/common"
	goservice "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
)

func RequiredRoles(_ goservice.ServiceContext, roles ...string) func(*gin.Context) {
	return func(c *gin.Context) {
		u := c.MustGet(common.CurrentUser).(common.Requester)

		for i := range roles {
			if u.GetRole() == roles[i] {
				c.Next()
				return
			}
		}

		panic(common.ErrNoPermission(nil))
	}
}

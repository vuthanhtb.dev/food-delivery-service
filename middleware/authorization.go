package middleware

import (
	"context"
	"errors"
	"fmt"
	"food-delivery-service/common"
	model "food-delivery-service/modules/user/model"
	goservice "food-delivery-service/plugin/gosdk"
	"food-delivery-service/plugin/tokenprovider"
	"github.com/gin-gonic/gin"
	"strings"
)

type AuthenticationStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.User, error)
}

func ErrWrongAuthHeader(err error) *common.AppError {
	return common.NewCustomError(
		err,
		fmt.Sprintf("wrong authen header"),
		fmt.Sprintf("ErrWrongAuthHeader"),
	)
}

func extractTokenFromHeaderString(s string) (string, error) {
	parts := strings.Split(s, " ")
	//"Authorization" : "Bearer {token}"

	if parts[0] != "Bearer" || len(parts) < 2 || strings.TrimSpace(parts[1]) == "" {
		return "", ErrWrongAuthHeader(nil)
	}

	return parts[1], nil
}

// RequiredAuth 1. Get token from header
// 2. Validate token and parse to payload
// 3. From the token payload, we use user_id to find from DB
func RequiredAuth(sc goservice.ServiceContext, authStore AuthenticationStore) func(c *gin.Context) {
	tokenProvider := sc.MustGet(common.JWTProvider).(tokenprovider.Provider)

	return func(c *gin.Context) {
		token, err := extractTokenFromHeaderString(c.GetHeader("Authorization"))

		if err != nil {
			panic(err)
		}

		//db := appContext.GetMaiDBConnection()
		//storage := userstore.NewSQLStore(db)
		//
		payload, err := tokenProvider.Validate(token)
		if err != nil {
			panic(err)
		}
		//
		//user, err := storage.FindUser(c.Request.Context(), map[string]interface{}{"id": payload.UserId})

		user, err := authStore.FindUser(c.Request.Context(), map[string]interface{}{"id": payload.UserId()})

		if err != nil {
			panic(err)
		}

		if user.Status == 0 {
			panic(common.ErrNoPermission(errors.New("user has been deleted or banned")))
		}

		user.Mask(common.DbTypeUser)

		c.Set(common.CurrentUser, user)
		c.Next()
	}
}

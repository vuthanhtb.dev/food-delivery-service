package handlers

import (
	"food-delivery-service/common"
	"food-delivery-service/middleware"
	restaurant "food-delivery-service/modules/restaurant/transportation/gin"
	restaurantlike "food-delivery-service/modules/restaurantlike/transportation/gin"
	storage "food-delivery-service/modules/user/storage"
	user "food-delivery-service/modules/user/transportation/gin"
	service "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

func MainRoute(router *gin.Engine, sc service.ServiceContext) {
	dbConn := sc.MustGet(common.DBMain).(*gorm.DB)
	userStore := storage.NewSQLStore(dbConn)

	v1 := router.Group("/v1")
	{
		v1.GET("/admin",
			middleware.RequiredAuth(sc, userStore),
			middleware.RequiredRoles(sc, "admin", "mod"),
			func(c *gin.Context) {
				c.JSON(http.StatusOK, gin.H{"data": 1})
			})

		users := v1.Group("/users")
		{
			users.POST("/register", user.Register(sc))
			users.POST("/login", user.Login(sc))
			users.GET("/profile", middleware.RequiredAuth(sc, userStore), user.Profile(sc))
		}

		restaurants := v1.Group("/restaurants", middleware.RequiredAuth(sc, userStore))
		{
			restaurants.POST("", restaurant.CreateRestaurantHandler(sc))
			restaurants.GET("", restaurant.ListRestaurant(sc))
			restaurants.GET("/:restaurant_id", restaurant.GetRestaurantHandler(sc))
			restaurants.PUT("/:restaurant_id", restaurant.UpdateRestaurantHandler(sc))
			restaurants.DELETE("/:restaurant_id", restaurant.DeleteRestaurantHandler(sc))

			restaurants.POST("/:restaurant_id/like", middleware.RequiredAuth(sc, userStore), restaurantlike.UserLikeRestaurant(sc))
			restaurants.DELETE("/:restaurant_id/dislike", middleware.RequiredAuth(sc, userStore), restaurantlike.UserDislikeRestaurant(sc))
			restaurants.GET("/:restaurant_id/liked-users", middleware.RequiredAuth(sc, userStore), restaurantlike.ListUsers(sc))
		}
	}
}

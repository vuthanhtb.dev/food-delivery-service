package handlers

import (
	userinternal "food-delivery-service/modules/user/transportation/internalgin"
	goservice "food-delivery-service/plugin/gosdk"
	"github.com/gin-gonic/gin"
)

func InternalRoute(router *gin.Engine, sc goservice.ServiceContext) {
	internal := router.Group("/internal")
	{
		internal.POST("/get-users-by-ids", userinternal.GetUserById(sc))
	}
}

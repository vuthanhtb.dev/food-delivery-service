package hasher

import (
	"crypto/md5"
	"encoding/hex"
)

type md5Hash struct{}

func NewMd5Hash() *md5Hash {
	return &md5Hash{}
}

func (h *md5Hash) Hash(data string) string {
	harsher := md5.New()
	harsher.Write([]byte(data))
	return hex.EncodeToString(harsher.Sum(nil))
}
